require 'optparse'

module OptExample
  VERSION = '1.0.0'
end

opta = 'default option'
OptionParser.new do |opt|
  opt.banner = "OptionParser example script."
  opt.version = OptExample::VERSION

  opt.on("-a ", '--arga', String, "argment a (default is '#{opta}')") { |o| opta = o }
  opt.parse!(ARGV)
end
p opta
